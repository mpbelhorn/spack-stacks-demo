# Issues

## Concretization info

Would like to be able to concretize with `-lINt` flags.

In `spack.yaml`, the syntax:

```
modules:
  enable::
    - tcl
    - lmod
```

raises an error on the second `:`. Is this a problem?

## Nested matrix resolution

It appears that nested matrixes don't work. For example, either:

```
spack:
  definitions:
    - core_compilers:
      -  '%gcc@4.8.5'
    - core_only_compute_packages:
      - matrix:
        - ['py-nose', 'py-numpy', 'py-mpi4py']
        - ['^python@2.7.15', '^python@3.7.0']
      - valgrind@3.11.0 +mpi~boost
    - manifest:
      - matrix:
        - [$core_only_compute_packages]
        - [$core_compilers]
  specs:
    - $manifest
```

```
spack:
  definitions:
    - core_compilers:
      -  '%gcc@4.8.5'
    - python:
      - matrix:
        - ['py-nose', 'py-numpy', 'py-mpi4py']
        - ['^python@2.7.15', '^python@3.7.0']
    - core_only_compute_packages:
      - $python
      - valgrind@3.11.0 +mpi~boost
    - manifest:
      - matrix:
        - [$core_only_compute_packages]
        - [$core_compilers]
  specs:
    - $manifest
```

gives rise to:

```
belhorn@login1.peak:.../spack/environments/peak-001 [features/stacks|OK]
$ spack concretize             
==> Error: 'dict' object has no attribute 'startswith'
belhorn@login1.peak:.../spack/environments/peak-001 [features/stacks|OK]
$ spack -d concretize
==> [2019-04-10-14:17:32.178901] Reading config file /autofs/nccs-svm1_sw/.testing/belhorn/spack/peak/stacks/etc/spack/defaults/modules.yaml
==> [2019-04-10-14:17:32.190481] Reading config file /autofs/nccs-svm1_sw/.testing/belhorn/spack/peak/stacks/etc/spack/defaults/linux/modules.yaml
==> [2019-04-10-14:17:32.194465] Reading config file /autofs/nccs-svm1_sw/.testing/belhorn/spack/peak/stacks/etc/spack/modules.yaml
==> [2019-04-10-14:17:32.221467] Reading config file /autofs/nccs-svm1_sw/.testing/belhorn/spack/peak/stacks/var/spack/environments/peak-001/spack.yaml
==> [2019-04-10-14:17:32.305962] Reading config file /autofs/nccs-svm1_sw/.testing/belhorn/spack/peak/stacks/etc/spack/defaults/config.yaml
==> [2019-04-10-14:17:32.321020] Reading config file /autofs/nccs-svm1_sw/.testing/belhorn/spack/peak/stacks/etc/spack/config.yaml
Traceback (most recent call last):
  File "/autofs/nccs-svm1_sw/.testing/belhorn/spack/peak/stacks/bin/spack", line 48, in <module>
    sys.exit(spack.main.main())
  File "/autofs/nccs-svm1_sw/.testing/belhorn/spack/peak/stacks/lib/spack/spack/main.py", line 675, in main
    return _invoke_command(command, parser, args, unknown)
  File "/autofs/nccs-svm1_sw/.testing/belhorn/spack/peak/stacks/lib/spack/spack/main.py", line 446, in _invoke_command
    return_val = command(parser, args)
  File "/autofs/nccs-svm1_sw/.testing/belhorn/spack/peak/stacks/lib/spack/spack/cmd/concretize.py", line 21, in concretize
    env.concretize(force=args.force)
  File "/autofs/nccs-svm1_sw/.testing/belhorn/spack/peak/stacks/lib/spack/spack/environment.py", line 679, in concretize
    self.user_specs, self.user_specs.specs_as_constraints):
  File "/autofs/nccs-svm1_sw/.testing/belhorn/spack/peak/stacks/lib/spack/spack/spec_list.py", line 54, in specs_as_constraints
    ordered_combo = sorted(combo, key=spec_ordering_key)
  File "/autofs/nccs-svm1_sw/.testing/belhorn/spack/peak/stacks/lib/spack/spack/spec_list.py", line 13, in spec_ordering_key
    if s.startswith('^'):
AttributeError: 'dict' object has no attribute 'startswith'
```

whereas

```
spack:
  definitions:
    - core_compilers:
      -  '%gcc@4.8.5'
    - python:
      - matrix:
        - ['py-nose', 'py-numpy', 'py-mpi4py']
        - ['^python@2.7.15', '^python@3.7.0']
        - ['%gcc@4.8.5']
    - core_only_compute_packages:
      - valgrind@3.11.0 +mpi~boost
    - manifest:
      - $python
      - matrix:
        - [$core_only_compute_packages]
        - [$core_compilers]
  specs:
    - $manifest
```
works fine.

## Dependency concretization infinite loop

Infinite loop for spec 'petsc%gcc@4.8.5~int64~metis+mpi~mumps~superlu-dist
^netlib-scalapack' where the requested build doesn't depend on scalapack. 

## rather than source file to activate env, make a modulefile.
